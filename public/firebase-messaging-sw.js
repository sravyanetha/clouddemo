// Scripts for firebase and firebase messaging
importScripts('https://www.gstatic.com/firebasejs/8.2.0/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/8.2.0/firebase-messaging.js');

// Initialize the Firebase app in the service worker by passing the generated config
const googleFcmConfig = {
  apiKey: "AIzaSyAkiAlYgLOa7rkl2neEt1tcYU1RupZTH1Y",
  authDomain: "bullitt-tracking-portal.firebaseapp.com",
  projectId: "bullitt-tracking-portal",
  storageBucket: "bullitt-tracking-portal.appspot.com",
  messagingSenderId: "744579071020",
  appId: "1:744579071020:web:da5c625a7a1f44e3f8a589",
  measurementId: "G-1H3GZLWVS9"
}

firebase.initializeApp(googleFcmConfig);

// Retrieve firebase messaging
const messaging = firebase.messaging();

messaging.onBackgroundMessage();