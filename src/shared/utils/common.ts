import { userSignOut } from "../../pages/authentication/services/authenticationService";
import {sessionType, localStorageNames } from "./Constants";
import moment from "moment";
import { BroadcastChannel } from "broadcast-channel";

const logoutChannel = new BroadcastChannel("logout");

export const toHoursAndMinutes = (totalMinutes) => {
  const hours = Math.floor(totalMinutes / 60);
  const minutes = totalMinutes % 60;
  const duration = hours + " Hours " + minutes + " mins"
  return duration;
}

export const isPrivateSession = (trackingSessionType:String)=>{
  if(trackingSessionType){
    return trackingSessionType.toLocaleUpperCase() === sessionType.private.toLocaleUpperCase()
  }
}

export const isPublicSession = (trackingSessionType:String)=>{
  if(trackingSessionType){
    return trackingSessionType.toLocaleUpperCase() === sessionType.public.toLocaleUpperCase()
  }
}

export const getDateTime = (date) => {
  const dateTime = new Date(date);
  const momentDate = moment(dateTime);
  const formattedDate = momentDate.format("lll");     
  return formattedDate;
}

export const getUTCDateTime = (date) => {
  const dateTime = new Date(date);    
  const momentDate = moment(dateTime);       
  const momentUTCDateFormat = momentDate.format("D MMMM YYYY");    
  const momentUTCTime = momentDate.format("LT");     
  const extractTime = moment(momentUTCTime, "hh:mm:ss A").format("HH:mm:ss");  
  const formatTime = extractTime?.slice(0,5);
  const formattedDate = formatTime + ", " + momentUTCDateFormat;  
  return formattedDate;
}

export const getStorageValue = (key:string) => {
  return JSON.parse(localStorage.getItem(key));
};

export const setStorageValue = (key:string, value:any) => {
  localStorage.setItem(key, JSON.stringify(value));
};

export const removeStorageValue = (key:string) => {
  localStorage.removeItem(key);
};

export const removeAllStorageValues = () => {
  localStorage.clear();
};

export const signOut = () => {
  userSignOut().then(() => {
    setStorageValue(localStorageNames?.currentPage, "");
    logoutChannel.postMessage("Logout");
    removeAllStorageValues();
    window.location.href = window.location.origin + "/";
  })
}

export const logoutAllTabs = () => {
  logoutChannel.onmessage = () => {    
    logoutChannel.close();
    window.location.href = window.location.origin + "/";     
  }
}

export const getBrowserName = (browser) => {
  switch (true) 
  {
  case (browser.indexOf("edg/") > -1): return "MS Edge";
  case (browser.indexOf("chrome") > -1 && browser.indexOf("edg/") < 1): return "Chrome";    
  case (browser.indexOf("firefox") > -1): return "Mozilla Firefox";
  case (browser.indexOf("safari") > -1): return "Safari";
  default: return "Other";
  }
}

export const isIosDevice = () => {
  if (window.navigator.userAgent.match(/iPad/i) || window.navigator.userAgent.match(/iPhone/i)) {
    // iPad or iPhone
    return "iOS";
  } else {
    return "Other";
  }
}