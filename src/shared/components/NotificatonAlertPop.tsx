import React from "react";
import { useTranslation } from "react-i18next";
import { Dialog } from "@mui/material";
import "./PopUp.css";

interface notificationAlertProps {
  isModelOpen?: boolean;
  closeModel?: any;
}

function NotificationAlertPop(props : notificationAlertProps) {
  const { t } = useTranslation();

  const handleClose = ()=>{
    props.closeModel()
  }

  return (
    <Dialog 
      open={props.isModelOpen}
      onClose={handleClose}
      className="notification_dialog"
      sx={{
        "& .MuiDialog-container": {
          "& .MuiPaper-root": {
            width: "100%",
            maxWidth: "20rem",
            borderRadius: "10px",
          },
        },
      }}
    >
      <div className="alert">
        <h3 className="popup_title">
          {t("notification_alert")}
        </h3>
        <p className="popup_message">{t("notification_alert_content")}</p>
        <p className="popup_message">{t("notification_alert_content_not_allow")}</p>
        <button className="dismiss_btn" onClick={() => props.closeModel()}>
          {t("ok")}
        </button>
      </div>
    </Dialog>
  );
}

export default NotificationAlertPop;
