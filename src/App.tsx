import "./App.css";
import React from "react";
import Header from "./shared/components/Header";
import { BrowserRouter, Navigate, Route, Routes } from "react-router-dom";
import Profile from "./pages/profile/components/Profile";
import Preferences from "./pages/preferences/components/Preferences";
import HistoryDetail from "./pages/profile/components/HistoryDetail";
import { useTranslation } from "react-i18next";
import LiveTracking from "./pages/liveTracking/components/LiveTracking";
import { pageNavPaths } from "./shared/utils/Config";
import Home from "./pages/home/components/Home";
import ErrorBoundary from "./shared/components/ErrorBoundary";
import Contacts from "./pages/contact/components/Contacts";
import { getStorageValue, setStorageValue, signOut } from "./shared/utils/common";
import { localStorageNames, mapTypes } from "./shared/utils/Constants";
import StatusMessage from "./shared/components/statusMessage";
import SessionExpiredPop from "./shared/components/SessoinExpiredPop";
import { requestForToken } from "./shared/components/FirebaseInit";
import { getPreferences } from "./pages/preferences/services/PreferenceServices";
import { apiResponseCodes } from "./shared/utils/Constants";
import { logoutAllTabs } from "./shared/utils/common";
import Login from "./pages/authentication/components/Login";
import ShareContent from "./shared/components/ShareContent";

function App() {
  
  const [lang, setLanguage] = React.useState("");
  const { i18n } = useTranslation();
  const languageContext = React.createContext("");
  const [isSessionExpired, setIsSessionExpired] = React.useState(false);
  const [loggedIn] = React.useState(getStorageValue(localStorageNames?.loggedIn));
  const [openLogin, setOpenLogin] = React.useState(false);

  const searchParams = new URLSearchParams(window.location?.search);
  const param = searchParams.get("return");
  
  const handleLanguageChange = (value) => {        
    setLanguage(value);  
    i18n.changeLanguage(value);        
  }

  React.useEffect(()=>{
    if (!loggedIn)
    {
      setStorageValue(localStorageNames?.language,"en");
      setStorageValue(localStorageNames?.mapType, mapTypes?.terrain);
    }
    else 
    {
      const loggedInUserMobile = JSON.parse(getStorageValue(localStorageNames?.loginDetails))?.mobileNo; 
      getPreferences(loggedInUserMobile).then((res) => {
        if (res && res?.result?.code === apiResponseCodes.success) {
          if (res?.result?.data)
          {
            setStorageValue(localStorageNames.language, res?.result?.data?.Language); 
            setStorageValue("i18nextLng", res?.result?.data?.Language); 
            handleLanguageChange(res?.result?.data?.Language); 
          }
          else
          {
            setStorageValue(localStorageNames.language, "en"); 
            setStorageValue("i18nextLng", "en"); 
            handleLanguageChange("en"); 
          }                     
        }
      })      
    }
    requestForToken();
  }, [])

  React.useEffect(() => {
    if (!loggedIn && param?.includes("profile")) {
      setOpenLogin(true);
    }
  }, [])

  React.useEffect(() => {     
    logoutAllTabs();    
  }, [])

  let timeoutHolder;
  
  const resetTimeout = () => {
    clearTimeout(timeoutHolder);
    timeoutHolder = setTimeout(logout, 30 * 60 * 1000); 
  };

  const logout = () => {
    setIsSessionExpired((current: boolean) => !current);
    signOut();
  };

  const handleClose = (event, reason) => {
    if (reason && reason === "backdropClick") 
      return;
    setIsSessionExpired((current: boolean) => !current);
  }

  const handleSignInClose = (event, reason) => {
    if (reason && reason === "backdropClick")
      return;
    setOpenLogin(false);
  }

  React.useEffect(() => {
    if (loggedIn)
    {
      timeoutHolder = setTimeout(logout, 30 * 60 * 1000);
      document.addEventListener("click", resetTimeout);
      document.addEventListener("keydown", resetTimeout);
    }
    
    return () => {
      document.removeEventListener("click", resetTimeout);
      document.removeEventListener("keydown", resetTimeout);
      clearTimeout(timeoutHolder);
    };
  }, []);
  
  return (
    <div className="App">  
      <BrowserRouter>
        <languageContext.Provider value={lang}>
          <div className="header_routes_container">
            <Header />             
            <ErrorBoundary>                          
              <Routes>
                {openLogin &&
                  <Route path={pageNavPaths?.home} element={ <>
                    <Home />
                    <Login handleClose={handleSignInClose} />
                  </>} />
                }
                <Route path={pageNavPaths?.home} element={ <Home /> } />
                <Route path={pageNavPaths?.shareImg} element={ <ShareContent /> } />
                <Route path={pageNavPaths?.liveTracking} element={ <LiveTracking /> } />
                <Route path={pageNavPaths?.profile} element={ loggedIn ? <Profile /> : <Navigate to="/" /> } />
                <Route path={pageNavPaths?.profileDetails} element={loggedIn ? <HistoryDetail /> : <Navigate to="/" />} />
                <Route path={pageNavPaths?.preferences} element={loggedIn ? <Preferences selectedLanguage={handleLanguageChange} /> : <Navigate to="/" /> } />
                <Route path={pageNavPaths?.contacts} element={loggedIn ? <Contacts/> : <Navigate to="/" /> } />         
                <Route path="*" element={<StatusMessage statusCode={404} />} />
              </Routes>  
            </ErrorBoundary>
          </div>
        </languageContext.Provider>        
      </BrowserRouter>  
      <SessionExpiredPop
        isModelOpen={isSessionExpired}
        closeModel={handleClose}
      />
    </div>
  );
}

export default App; 