import { getAxiosHeaders, requestMaker } from "../../../shared/providers/requestMaker";
import { apiPath } from "../../../shared/utils/apiConfig";
import { authURl } from "../../../shared/utils/Config";
import { REQUEST_TYPES } from "../../../shared/utils/Constants"

export const userSignIn = async (data) => {
  const response = await requestMaker({
    method: REQUEST_TYPES?.POST,
    url: `${authURl}${apiPath?.USER_SIGN_IN}`,
    headers: getAxiosHeaders(true, false, false),
    data: data,
  });
  return response;
};

export const userSignOut = async () => {
  const response = await requestMaker({
    method: REQUEST_TYPES?.POST,
    url: `${authURl}${apiPath?.USER_SIGN_OUT}`,
    headers: getAxiosHeaders(true, false, true),
  });
  return response;
};
